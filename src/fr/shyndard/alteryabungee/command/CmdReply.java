package fr.shyndard.alteryabungee.command;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.function.MsgManager;
import fr.shyndard.alteryabungee.function.MuteManager;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdReply extends Command {
	
	public Main plugin;

	public CmdReply(Main Nethad) {
		super("reply", null, "r", "repondre");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		if(MuteManager.isMute(player)) {
			player.sendMessage(ChatColor.RED + "Vous �tes r�duit au silence jusqu'au " + MuteManager.getRemainingTime(player));
			return;
		}
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(pi.getRank().getPower() == 10 || !pi.hasPermission("message.send")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas acc�s � cette commande.");
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.RED + "Utilisation /r <message>");
			return;
		}
		ProxiedPlayer target = MsgManager.getLastMsgTarget(player);
		if(target == null) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez personne � qui r�pondre.");
			return;
		}
		PlayerInformation pit = Data.player_list.get(target.getUniqueId());
		String message = args[0];
		for(int i = 1; i < args.length; i++) message += " " + args[i];
		MsgManager.sendMessageTo(player, target);
		GlobalFunction.addLog(player, target.getName()+" : "+message, true);
		target.sendMessage("[" + pi.getRank().getColor() + player.getName() + ChatColor.GOLD + " -> " + pit.getRank().getColor() + "moi" + ChatColor.WHITE + "] " + message);
		sender.sendMessage("[" + pi.getRank().getColor() + "moi" + ChatColor.GOLD + " -> " + pit.getRank().getColor() + target.getName() + ChatColor.WHITE + "] " + message);
	}
}
