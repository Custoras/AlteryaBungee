package fr.shyndard.alteryabungee.command;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdKick extends Command {
	
	public Main plugin;

	public CmdKick(Main Nethad) {
		super("kick");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.hasPermission("staff.kick")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		if(args.length >= 2) {
			ProxiedPlayer target = BungeeCord.getInstance().getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage(ChatColor.RED + "Ce joueur est introuvable.");
				return;
			}
			if(Data.player_list.get(target.getUniqueId()).getRank().getPower() <= pi.getRank().getPower()) {
				sender.sendMessage(ChatColor.RED + "Vous ne pouvez expulser ce joueur.");
				return;
			}
			StringBuilder kick_reason = new StringBuilder();
			kick_reason.append(args[1]);
			for(int i = 2; i<args.length; i++) {
				kick_reason.append(args[i]);
			}
			target.disconnect(kick_reason.toString());
			GlobalFunction.sendMessageToAllStaff(ChatColor.YELLOW + target.getName() + ChatColor.GRAY + " a �t� �ject� par " + player.getName() + ".");
		} else {
			sender.sendMessage(ChatColor.GRAY + "/kick <pseudo> <raison>");
		}
	}

}
