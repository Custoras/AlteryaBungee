package fr.shyndard.alteryabungee.command;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.BanManager;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdBan extends Command {
	
	public Main plugin;

	public CmdBan(Main Nethad) {
		super("ban");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.hasPermission("staff.ban")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		if(args.length >= 3) {
			ProxiedPlayer target = BungeeCord.getInstance().getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage(ChatColor.RED + "Ce joueur est introuvable.");
				return;
			}
			if(Data.player_list.get(target.getUniqueId()).getRank().getPower() <= pi.getRank().getPower()) {
				sender.sendMessage(ChatColor.RED + "Vous ne pouvez bannir ce joueur.");
				return;
			}
			long time = GlobalFunction.getTime(args[1]);
			if(time == -1) {
				sender.sendMessage(ChatColor.RED + "Veuillez saisir une date valide (format <temps><s|m|h|j|M>)");
				return;
			}
			String reason = args[2];
			for(int i = 3; i < args.length; i++) reason += " " + args[i];
			BanManager.add(target, time, reason, player);
			GlobalFunction.sendMessageToAllStaff(ChatColor.YELLOW + target.getName() + ChatColor.GRAY + " a �t� banni par " + player.getName() + " (" + args[1] + ") pour " + reason);
			target.disconnect(ChatColor.RED + "Vous �tes bannir de ce serveur\nRaison : " + reason);
		} else {
			sender.sendMessage(ChatColor.GRAY + "/ban <pseudo> <temps><s|m|h|j|M> <raison>");
		}
	}

}
