package fr.shyndard.alteryabungee.command;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.UUID;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdLierDiscord extends Command {
	
	public Main plugin;

	public CmdLierDiscord(Main Nethad) {
		super("lierdiscord");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.hasPermission("lierdiscord.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		if(alreadyLink(player.getUniqueId())) {
			sender.sendMessage(ChatColor.RED + "Votre compte discord est d�j� li�.");
			return;
		}
		String key = getKey(player.getUniqueId());
		if(key == null) {
			char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPKRSTUVWXYZ".toCharArray();
			StringBuilder sb = new StringBuilder();
			Random random = new Random();
			for (int i = 0; i < 4; i++) {
			    char c = chars[random.nextInt(chars.length)];
			    sb.append(c);
			}
			key = sb.toString();
			if(!setDiscordLink(player.getUniqueId(), key)) {
				sender.sendMessage(ChatColor.RED + "Impossible de lier votre compte. Recommencez plus tard.");
				return;
			}
		}
		sender.sendMessage(ChatColor.GREEN + "Cl� discord : " + ChatColor.YELLOW + key);
		
	}
	
	static boolean setDiscordLink(UUID uuid, String key) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO web.discord_link VALUES ((SELECT id FROM minecraft.player_information WHERE uuid = '"+uuid+"'), NULL, '"+key+"')");
			state.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean alreadyLink(UUID uuid) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM web.discord_link WHERE player_id = (SELECT id FROM minecraft.player_information WHERE uuid = '"+uuid+"') AND discord_id IS NOT NULL AND link_key IS NULL");
			while(result.next()) return true;
		 } catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public static String getKey(UUID uuid) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT link_key FROM web.discord_link WHERE player_id = (SELECT id FROM minecraft.player_information WHERE uuid = '"+uuid+"')");
			while(result.next()) return result.getString("link_key");
		 } catch (SQLException e) {e.printStackTrace();}
		return null;
	}

}
