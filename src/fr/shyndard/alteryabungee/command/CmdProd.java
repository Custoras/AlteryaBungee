package fr.shyndard.alteryabungee.command;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdProd extends Command {
	
	public Main plugin;

	public CmdProd(Main Nethad) {
		super("prod");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.isStaff()) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		player.connect(BungeeCord.getInstance().getServerInfo("prod"));
	}

}
