package fr.shyndard.alteryabungee.command;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdTChat extends Command {
	
	public Main plugin;

	public CmdTChat(Main Nethad) {
		super("tchat");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.hasPermission("staff.tchat")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		if(Data.tchat_player_list.contains(player.getUniqueId())) {
			player.sendMessage(ChatColor.RED + "[TC] " + ChatColor.GRAY + "Surveillance du t'chat " + ChatColor.RED + "d�sactiv�e" + ChatColor.GRAY + ".");
			Data.tchat_player_list.remove(player.getUniqueId());
		} else {
			player.sendMessage(ChatColor.RED + "[TC] " + ChatColor.GRAY + "Surveillance du t'chat " + ChatColor.GREEN + "activ�e" + ChatColor.GRAY + ".");
			Data.tchat_player_list.add(player.getUniqueId());
		}
	}
}
