package fr.shyndard.alteryabungee.command;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdHelp extends Command {
	
	public Main plugin;

	public CmdHelp(Main Nethad) {
		super("help", null, "aide");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		player.sendMessage(Main.getLine(ChatColor.GREEN));
		if(pi.getRank().getPower() == 10) {
			sender.sendMessage(ChatColor.GREEN + "Vous commencer, inscrivez vous via " + ChatColor.GREEN + "/lier");
		} else {
			sender.sendMessage(ChatColor.GREEN + "/position" + ChatColor.GRAY + " permet de connaitre votre position, g�rer l'achat de parcelle et de claim.");
			sender.sendMessage(ChatColor.GRAY + "Pour plus d'informations : " + ChatColor.DARK_AQUA + "https://alterya.eu/forum");
		}
		player.sendMessage(Main.getLine(ChatColor.GREEN));
	}
}
