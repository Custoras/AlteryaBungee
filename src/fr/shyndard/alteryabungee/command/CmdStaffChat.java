package fr.shyndard.alteryabungee.command;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdStaffChat extends Command {
	
	public Main plugin;
	private String prefix = ChatColor.RED + "[SC] " + ChatColor.GREEN;

	public CmdStaffChat(Main Nethad) {
		super("sc", null, "staffchat");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.hasPermission("staff.chat")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		if(args.length == 0) {
			sender.sendMessage(ChatColor.GRAY + "Utilisation : /sc <message>");
		} else {
			String message = args[0];
			for(int i = 1; i < args.length; i++) message += " " + args[i];
			GlobalFunction.sendMessageToAllStaff(prefix + pi.getRank().getColor() + player.getName()  + ChatColor.GREEN + "> " + message);
		}
	}
}
