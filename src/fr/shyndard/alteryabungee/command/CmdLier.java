package fr.shyndard.alteryabungee.command;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdLier extends Command {
	
	public Main plugin;

	public CmdLier(Main Nethad) {
		super("lier", null, "enregistrement");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	  
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(sender instanceof ProxyServer) {
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.hasPermission("lier.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		if(accountExist(player)) {
			player.sendMessage(ChatColor.GREEN + "Vous �tes d�j� enregistr� � l'hotel de ville.");
			return;
		}
		String key = linkExist(player);
		if(key == null) key = generateKey(player);
		TextComponent message = new TextComponent(ChatColor.YELLOW + "Terminez votre enregistrement en cliquant sur ce message.");
		message.setHoverEvent( new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder( ChatColor.AQUA + "Acc�der au site pour vous inscrire.").create()));
		message.setClickEvent( new ClickEvent(ClickEvent.Action.OPEN_URL, "http://alterya.eu/inscription/" + key + "/"));
		player.sendMessage(message);
	}
	
	public boolean accountExist(ProxiedPlayer player) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM player_information, web.account_information WHERE id = player_id AND uuid = '" + player.getUniqueId() + "'");
			while(result.next()) {
        		return true;
        	}
		 } catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public String linkExist(ProxiedPlayer player) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT url_key FROM player_information, web.inscription_key WHERE id = player_id AND uuid = '" + player.getUniqueId() + "'");
			while(result.next()) {
        		return result.getString("url_key");
        	}
		 } catch (SQLException e) {e.printStackTrace();}
		return null;
	}
	
	public String generateKey(ProxiedPlayer player) {
		String key = UUID.randomUUID().toString().replaceAll("-", "");
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO web.inscription_key VALUES ('"+key+"', (SELECT id FROM minecraft.player_information WHERE uuid = '"+player.getUniqueId()+"'))");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
		return key;
	}
}