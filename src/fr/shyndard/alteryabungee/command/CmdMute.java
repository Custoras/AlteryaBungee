package fr.shyndard.alteryabungee.command;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.function.MuteManager;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CmdMute extends Command {
	
	public Main plugin;

	public CmdMute(Main Nethad) {
		super("mute");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.hasPermission("staff.mute")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		if(args.length >= 2) {
			ProxiedPlayer target = BungeeCord.getInstance().getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage(ChatColor.RED + "Ce joueur est introuvable.");
				return;
			}
			if(Data.player_list.get(target.getUniqueId()).getRank().getPower() <= pi.getRank().getPower()) {
				sender.sendMessage(ChatColor.RED + "Vous ne pouvez r�duire au silence ce joueur.");
				return;
			}
			long time = GlobalFunction.getTime(args[1]);
			if(time == -1) {
				sender.sendMessage(ChatColor.RED + "Veuillez saisir une date valide (format <temps><s|m|h|j|M>)");
				return;
			}
			MuteManager.add(target, time);
			GlobalFunction.sendMessageToAllStaff(ChatColor.YELLOW + target.getName() + ChatColor.GRAY + " a �t� r�duit au silence par " + player.getName() + " (" + args[1] + ").");
			target.sendMessage(ChatColor.RED + "[Notif] " + ChatColor.GRAY + "Vous �tes r�duits au silence jusqu'au " + MuteManager.getRemainingTime(target) + ".");
		} else {
			sender.sendMessage(ChatColor.GRAY + "/mute <pseudo> <temps><s|m|h|j|M>");
		}
	}

}
