package fr.shyndard.alteryabungee.command;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

import fr.shyndard.alteryabungee.APIConnection;
import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import fr.shyndard.alteryabungee.method.RankInformation;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

@SuppressWarnings("deprecation")
public class CmdStaff extends Command {
	
	String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Staff" + ChatColor.DARK_GRAY + "] ";
	public Main plugin;

	public CmdStaff(Main Nethad) {
		super("staff");
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerCommand(plugin, this);      
	}
	
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof ProxiedPlayer)) {
			sender.sendMessage(ChatColor.RED + "Commande inaccessible via la console.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)sender;
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		if(!pi.hasPermission("staff.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return;
		}
		if(args.length > 0) {
			if(args[0].equalsIgnoreCase("addplayerpermission") || args[0].equalsIgnoreCase("addpp")) {
				if(pi.getRank().getPower() > 1 && !pi.hasPermission("staff.addplayerpermission")) {
					sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
					return;
				}
				if(args.length == 3) {
					UUID target_uuid = GlobalFunction.getPlayerUUID(args[1]);
					if(target_uuid == null) {
						sender.sendMessage(ChatColor.RED + "Ce joueur est introuvable.");
						return;
					}
					if(!pi.hasPermission(args[2].toLowerCase())) {
						addPermission(target_uuid, args[2]);
						ProxiedPlayer target = BungeeCord.getInstance().getPlayer(target_uuid);
						if(target != null) {
							Data.player_list.get(target.getUniqueId()).actualize(true);
						}
						sender.sendMessage(ChatColor.GRAY + "Permission " + ChatColor.GREEN + args[2] + ChatColor.GRAY + " ajout�e � " + ChatColor.YELLOW + args[1] + ChatColor.GRAY + ".");
					} else {
						sender.sendMessage(ChatColor.RED + "Ce joueur a d�j� cette permission.");
					}
				} else {
					sender.sendMessage(ChatColor.GRAY + "/staff addpermission <pseudo> <permission>");
				}
			}
			else if(args[0].equalsIgnoreCase("setrank")) {
				if(pi.getRank().getPower() > 1 && !pi.hasPermission("staff.setrank")) {
					sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
					return;
				}
				if(args.length == 3) {
					int rank_id = getRankId(args[2]);
					RankInformation target_ri = Data.rank_list.get(rank_id);
					if(rank_id == 0 || target_ri == null) {
						sender.sendMessage(ChatColor.RED + "Ce grade n'existe pas.");
						return;
					}
					UUID target_uuid = GlobalFunction.getPlayerUUID(args[1]);
					if(target_uuid == null) {
						sender.sendMessage(ChatColor.RED + "Ce joueur est introuvable.");
						return;
					}
					setRank(target_uuid, rank_id);
					sender.sendMessage(ChatColor.GREEN + "Grade " + target_ri.getColor() + target_ri.getName() + ChatColor.GREEN + " attribu� � " + ChatColor.YELLOW + args[1] + ChatColor.GREEN + ".");
					ProxiedPlayer target = BungeeCord.getInstance().getPlayer(target_uuid);
					if(target == null) return;
					Data.player_list.get(target.getUniqueId()).actualize(true);
				} else sender.sendMessage(ChatColor.GRAY + "/staff setrank <pseudo> <grade>");
			}
			else if(args[0].equalsIgnoreCase("load")) {
				if(args.length == 2) {
					if(args[1].equalsIgnoreCase("player")) {
						for(PlayerInformation pi_value : Data.player_list.values()) {
							pi_value.actualize(false);
						}
						APIConnection.send("actualizeplayer", null);
						sender.sendMessage(prefix + ChatColor.GREEN + "Actualisation joueur effectu�e.");
					} else if(args[1].equalsIgnoreCase("rank")) {
						for(RankInformation ri_value : Data.rank_list.values()) {
							ri_value.actualize();
						}
						APIConnection.send("actualizerank", null);
						sender.sendMessage(prefix + ChatColor.GREEN + "Actualisation grade effectu�e.");
					} else sender.sendMessage(ChatColor.GRAY + "/staff load <player|rank>");
				} else sender.sendMessage(ChatColor.GRAY + "/staff load <player|rank>");
			}
			else if(args[0].equalsIgnoreCase("info")) {
				if(pi.getRank().getPower() > 1 && !pi.hasPermission("staff.info")) {
					sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
					return;
				}
				if(args.length == 2) {
					String pseudo = GlobalFunction.getExactPlayerName(args[1]);
					if(pseudo == null) {
						sender.sendMessage(ChatColor.RED + "Ce joueur est introuvable.");
						return;
					}
					RankInformation ri = Data.rank_list.get(GlobalFunction.getPlayerRankId(pseudo));
					String[] player_connection_date = getPlayerConnectionDate(pseudo).split("!");
					sender.sendMessage(GlobalFunction.getLine(ChatColor.GREEN));
					sender.sendMessage(ChatColor.GRAY + "Pseudo : " + ChatColor.GREEN + pseudo);
					sender.sendMessage(ChatColor.GRAY + "Grade : " + ChatColor.GREEN + ri.getColor() + ri.getName());
					sender.sendMessage(ChatColor.GRAY + "Premi�re connexion : " + ChatColor.GREEN + player_connection_date[0]);
					sender.sendMessage(ChatColor.GRAY + "Derni�re connexion : " + ChatColor.GREEN + player_connection_date[1]);
					sender.sendMessage(GlobalFunction.getLine(ChatColor.GREEN));
				} else sender.sendMessage(ChatColor.GRAY + "/staff info <pseudo>");
			} else sendHelp(sender);
		} else sendHelp(sender);
	}
	
	private static void sendHelp(CommandSender sender) {
		sender.sendMessage("Commandes staff disponibles");
		sender.sendMessage("/staff info <pseudo>");
		sender.sendMessage("/staff setrank <player> <permission>");
		sender.sendMessage("/staff setrank <player> <rang>");
		sender.sendMessage("/staff load <player|rank>");
	}
	
	private void addPermission(UUID uuid, String permission) {
		try {
			PreparedStatement stmt = Main.getSql().getConnection().prepareStatement("INSERT INTO player_permission VALUES (? , (SELECT id FROM player_information WHERE uuid = '" + uuid + "'))");
			    stmt.setString(1, permission.toLowerCase());
			    stmt.executeUpdate();
			    stmt.close();
		} catch (SQLException e) {e.printStackTrace();}	
	}
	
	private void setRank(UUID uuid, Integer rank) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			state.executeUpdate("UPDATE `player_information` SET `rank_id` = "+rank+" WHERE `uuid` = '"+uuid.toString()+"'");
			state.close();
		 } catch (SQLException e) {e.printStackTrace();}
	}
	
	private int getRankId(String name) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT id, name FROM `rank_information` WHERE `name` LIKE '%" + name + "%' OR id ='" + name + "'");
	        try{
	        	while(result.next()) {
	        		return result.getInt("id");
	        	}
	        } catch (SQLException e) {e.printStackTrace();}
		} catch (SQLException e) {e.printStackTrace();}
		return 0;
	}
	
	public static String getPlayerConnectionDate(String player) {
		try {
			PreparedStatement state = Main.getSql().getConnection().prepareStatement("SELECT first_connection, last_connection FROM player_information WHERE name = ?");
			state.setString(1, player);
			ResultSet result = state.executeQuery();
			while(result.next()) {				
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeZone(TimeZone.getDefault());
				
				int day;
				int month;
				
				String date1;
				calendar.setTimeInMillis(result.getLong("first_connection") * 1000);
				day = calendar.get(Calendar.DATE);
				month = calendar.get(Calendar.MONTH) + 1;
				date1 = (day < 10 ? "0" : "") + day + "/" + (month < 10 ? "0" : "") + month + "/" + calendar.get(Calendar.YEAR) + " � " + calendar.get(Calendar.HOUR_OF_DAY) + "h" + calendar.get(Calendar.MINUTE);
				
				String date2;
				calendar.setTimeInMillis(result.getLong("last_connection") * 1000);
				day = calendar.get(Calendar.DATE);
				month = calendar.get(Calendar.MONTH) + 1;
				date2 = (day < 10 ? "0" : "") + day + "/" + (month < 10 ? "0" : "") + month + "/" + calendar.get(Calendar.YEAR) + " � " + calendar.get(Calendar.HOUR_OF_DAY) + "h" + calendar.get(Calendar.MINUTE);

				return date1 + "!" + date2;
			}
		} catch (SQLException e) {e.printStackTrace();}
		return null;
	}

}
