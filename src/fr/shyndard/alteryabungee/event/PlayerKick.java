package fr.shyndard.alteryabungee.event;

import fr.shyndard.alteryabungee.Main;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerKick implements Listener {
	
	Main plugin;
	
	public PlayerKick(Main Nethad) {
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerListener(plugin, this);    
	}
	
	@EventHandler
	public void onKick(ServerKickEvent event) {
		if(event.getPlayer() == null || event.getPlayer().getServer() == null || !event.getPlayer().isConnected()) return;
		event.setCancelled(true);
		for(ServerInfo si : BungeeCord.getInstance().getServers().values()) {
			if(!si.equals(event.getPlayer().getServer().getInfo()) && (si.getName().equals("event") || si.getName().equals("semi-rp"))) {
				event.setCancelServer(si);
				return;
			}
		}
	}
}
