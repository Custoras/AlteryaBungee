package fr.shyndard.alteryabungee.event;

import java.util.ArrayList;
import java.util.List;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.function.MuteManager;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

@SuppressWarnings("deprecation")
public class PlayerChat implements Listener {

	Main plugin;
	List<String> blocked_cmd = new ArrayList<>();
	
	public PlayerChat(Main Nethad) {
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerListener(plugin, this);
	    blocked_cmd.add("warp");
	    blocked_cmd.add("me");
	    blocked_cmd.add("back");
	    blocked_cmd.add("clear");
	    blocked_cmd.add("world");
	    blocked_cmd.add("version");
	    blocked_cmd.add("plugins");
	    blocked_cmd.add("pl");
	}
	
	@EventHandler
	public void onChat(ChatEvent event) {
		if(!(event.getSender() instanceof ProxiedPlayer)) {
			System.out.println("Vous devez �tre un joueur.");
			return;
		}
		ProxiedPlayer player = (ProxiedPlayer)event.getSender();
		PlayerInformation pi = Data.player_list.get(player.getUniqueId());
		String command = event.getMessage().toLowerCase().split(" ")[0];
		if(command.startsWith("/")) {
			if((!pi.isStaff() && blocked_cmd.contains(command.substring(1))) || (pi.getRank().getPower() == 10 && (!command.substring(1).equalsIgnoreCase("tutoriel") && !command.substring(1).equalsIgnoreCase("lier")))) {
				player.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
				event.setCancelled(true);
				return;
			}
		}
		else if(MuteManager.isMute(player)) {
			player.sendMessage(ChatColor.RED + "L'acc�s au t'chat vous est temporairement interdit.");
			event.setCancelled(true);
			return;
		} else {
			GlobalFunction.addLog(player, event.getMessage(), false);
			for(ProxiedPlayer staff : plugin.getProxy().getPlayers()) {
				if(Data.tchat_player_list.contains(staff.getUniqueId())) {
					if(!staff.getServer().getInfo().getName().equals(player.getServer().getInfo().getName())) {
						staff.sendMessage(ChatColor.RED + "[TC] " + ChatColor.GRAY + player.getName() + ": " + event.getMessage());
					}
				}
			}
		}
	}
}
