package fr.shyndard.alteryabungee.event;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.BanManager;
import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.method.BanNote;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerConnection implements Listener {

	Main plugin;	
	List<UUID> connected = new ArrayList<>();
	
	
	public PlayerConnection(Main Nethad) {
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerListener(plugin, this);    
	}
	
	@EventHandler
	public void playerPreLogin(PreLoginEvent event) {
		if(event.getConnection().getVersion() < 107 || event.getConnection().getVersion() > 340) {
			event.setCancelled(true);
			TextComponent message = new TextComponent("Connectez vous en version 1.9 minimum (1.12.2 recommand�e)");
			message.setColor(ChatColor.RED);
			message.setBold(true);
			event.setCancelReason(message);
			return;
		} else if(!GlobalFunction.isServerRpOpen()) {
			event.setCancelled(true);	
			TextComponent message = new TextComponent("Le serveur est temporairement indisponible\nVeuillez patienter quelques instants puis recommencer");
			message.setColor(ChatColor.RED);
			message.setBold(true);
			event.setCancelReason(message);
		}
	}
	
	@EventHandler
	public void playerPostLogin(PostLoginEvent event) {
		ProxiedPlayer player = event.getPlayer();
		if(connected.contains(player.getUniqueId())) {
			TextComponent message = new TextComponent("Ne vous reconnectez pas trop vite.");
			message.setColor(ChatColor.RED);
			message.setBold(true);
			event.getPlayer().disconnect(message);
		} else if(BanManager.isBan(event.getPlayer())) {
			BanNote bn = BanManager.getBanNote(player);
			TextComponent message = new TextComponent("Vous �tes banni de ce serveur\nRaison : "+bn.getReason()+"\nDate de fin : "+bn.getRemaining());
			message.setColor(ChatColor.RED);
			message.setBold(true);
			event.getPlayer().disconnect(message);
		} else {
			GlobalFunction.updatePlayer(player, 1);
			Data.player_list.put(player.getUniqueId(), new PlayerInformation(event.getPlayer()));
			connected.add(player.getUniqueId());
			BungeeCord.getInstance().broadcast("[" + ChatColor.GREEN + "+" + ChatColor.WHITE + "] " + Data.player_list.get(event.getPlayer().getUniqueId()).getRank().getColor() + event.getPlayer().getName() + ChatColor.GRAY + " nous a rejoint.");
		}
	}
	
	@EventHandler
	public void playerDisconnect(PlayerDisconnectEvent event) {
		ProxiedPlayer player = event.getPlayer();
		BungeeCord.getInstance().getScheduler().schedule(Main.getPlugin(), 
			new Runnable() {
				@Override
				public void run() {
					if(connected.contains(player.getUniqueId()) && !player.isConnected()) {
						connected.remove(player.getUniqueId());
						GlobalFunction.updatePlayer(player, 0);
						BungeeCord.getInstance().broadcast("[" + ChatColor.RED + "-" + ChatColor.WHITE + "] " + Data.player_list.get(event.getPlayer().getUniqueId()).getRank().getColor() + event.getPlayer().getName() + ChatColor.GRAY + " nous a quitt�.");
						Data.player_list.remove(event.getPlayer().getUniqueId());
					}
				}
			}  , 1, TimeUnit.SECONDS);
	}
}
