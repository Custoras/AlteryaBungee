package fr.shyndard.alteryabungee.event;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing.Protocol;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PingAction implements Listener {

	Main plugin;	
	
	public PingAction(Main Nethad) {
	    plugin = Nethad;
	    plugin.getProxy().getPluginManager().registerListener(plugin, this);    
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPing(ProxyPingEvent event) {
		if(GlobalFunction.isServerRestricted()) {
			event.getResponse().setDescription(ChatColor.translateAlternateColorCodes('&', GlobalFunction.getServerDescription()));
			event.getResponse().setVersion(new Protocol("Maitenance", 1000));
		} else {
			event.getResponse().setDescription(ChatColor.translateAlternateColorCodes('&', GlobalFunction.getServerDescription()));
		}
	}
}
