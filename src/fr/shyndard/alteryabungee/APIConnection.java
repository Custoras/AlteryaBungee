package fr.shyndard.alteryabungee;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class APIConnection implements Listener {

	static String channel = "alterya";
	
	@EventHandler
    public void onPluginMessage(PluginMessageEvent ev) throws IOException {
        if (!ev.getTag().equals(channel)) {
            return;
        }
        if (!(ev.getSender() instanceof Server)) {
            return;
        }
        ByteArrayInputStream stream = new ByteArrayInputStream(ev.getData());
	    DataInputStream in = new DataInputStream(stream);
        String[] args = in.readUTF().split("::");
		if(args.length == 0) return;
	}
	
	public static void send(String message, ServerInfo server) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
			out.writeUTF(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
        if(server == null) BungeeCord.getInstance().getServerInfo("semi-rp").sendData(channel, stream.toByteArray());
        else server.sendData(channel, stream.toByteArray());
    }
}
