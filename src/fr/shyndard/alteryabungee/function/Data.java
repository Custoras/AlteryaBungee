package fr.shyndard.alteryabungee.function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import fr.shyndard.alteryabungee.method.PlayerInformation;
import fr.shyndard.alteryabungee.method.RankInformation;

public class Data {

	public static Map<UUID, PlayerInformation> player_list = new HashMap<>();
	public static Map<Integer, RankInformation> rank_list = new HashMap<>();
	
	public static List<UUID> tchat_player_list = new ArrayList<>();

}
