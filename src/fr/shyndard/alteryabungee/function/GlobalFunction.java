package fr.shyndard.alteryabungee.function;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.method.RankInformation;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;

@SuppressWarnings("deprecation")
public class GlobalFunction {

	public static String getLine(ChatColor color) {
		return ("" + color + ChatColor.STRIKETHROUGH + "                                                                                ");
	}
	
	public static void updatePlayer(ProxiedPlayer player, int connected) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			if(playerExist(player)) state.executeUpdate("UPDATE player_information SET name='"+player.getName()+"', address='"+player.getAddress().getAddress().toString().substring(1)+"', last_connection='"+(System.currentTimeMillis()/1000)+"', connected="+connected+" WHERE uuid = '"+player.getUniqueId()+"'");
			else {
				state.executeUpdate("INSERT INTO `player_information`(`uuid`, `name`, `address`, `first_connection`, `last_connection`, `connected`) VALUES ('"+player.getUniqueId()+"', '"+player.getName()+"', '"+player.getAddress().getAddress().toString().substring(1)+"', '"+(System.currentTimeMillis()/1000)+"', '"+(System.currentTimeMillis()/1000)+"', 1)");
				state.executeUpdate("INSERT INTO quest_player VALUES ((SELECT MAX(id) FROM player_information), 1, 0, NULL)");
			}
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static String getServerDescription() {
		try {
			String request = "SELECT value FROM system_parameter WHERE parameter = ";
			if(isServerRestricted()) request += "'proxy_description_maintenance'";
			else if(!isServerRpOpen()) request += "'proxy_description_down'";
			else request += "'proxy_description'";
			
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery(request);
			while(result.next()) return result.getString("value");
		 } catch (SQLException e) {e.printStackTrace();}
		return ChatColor.LIGHT_PURPLE + "[ Alterya : Acc�s temporairement bloqu� ]";
	}
	
	public static boolean isServerRestricted() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM system_parameter WHERE parameter = 'restrict_access' AND value = 1");
			while(result.next()) {
        		return true;
        	}
		 } catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public static boolean playerExist(ProxiedPlayer player) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM player_information WHERE uuid = '" + player.getUniqueId() + "'");
			while(result.next()) {
        		return true;
        	}
		 } catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public static void loadRank() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT id FROM rank_information");
			while(result.next()) {
				new RankInformation(result.getInt("id"));
        	}
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void sendMessageToAllStaff(String message) {
		for(ProxiedPlayer players : BungeeCord.getInstance().getPlayers()) {
			if(Data.player_list.get(players.getUniqueId()).isStaff()) {
				players.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Staff" + ChatColor.DARK_GRAY + "] " + ChatColor.GRAY + message);
			}
		}
	}
	
	public static UUID getPlayerUUID(String player) {
		try {
			PreparedStatement state = Main.getSql().getConnection().prepareStatement("SELECT uuid FROM player_information WHERE name = ?");
			state.setString(1, player);
			ResultSet result = state.executeQuery();
			while(result.next()) {
				return UUID.fromString(result.getString("uuid"));
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return null;
	}
	
	public static String getExactPlayerName(String player) {
		try {
			PreparedStatement state = Main.getSql().getConnection().prepareStatement("SELECT name FROM player_information WHERE name = ?");
			state.setString(1, player);
			ResultSet result = state.executeQuery();
			while(result.next()) {
				return result.getString("name");
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return null;
	}
	
	public static int getPlayerRankId(String player) {
		try {
			PreparedStatement state = Main.getSql().getConnection().prepareStatement("SELECT rank_id FROM player_information WHERE name = ?");
			state.setString(1, player);
			ResultSet result = state.executeQuery();
			while(result.next()) {
				return result.getInt("rank_id");
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return 15;
	}

	public static boolean canAccessServer(UUID uuid) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM player_information AS PI JOIN rank_information AS RI ON PI.rank_id = RI.id JOIN system_parameter AS SP ON RI.power <= SP.value WHERE parameter = 'restrict_access_rank' AND PI.uuid = '"+uuid+"';");
			while(result.next()) {
				return true;
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}

	public static void addLog(ProxiedPlayer sender, String message, boolean isprivate) {
		try {
			String sql = "INSERT INTO chat_log VALUES (NULL, (SELECT id FROM player_information WHERE uuid = ?), ?, ?, ?)";
			PreparedStatement preparedStatement = Main.getSql().getConnection().prepareStatement(sql);
			preparedStatement.setString(1, sender.getUniqueId().toString());
			preparedStatement.setString(2, message);
			preparedStatement.setFloat(3, (System.currentTimeMillis()/1000));
			preparedStatement.setBoolean(4, isprivate);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static long getTime(String arg) {
		try {
			char lastChar = arg.charAt(arg.length()-1);
			long time = Integer.parseInt(arg.substring(0, arg.length() - 1));
			switch(lastChar) {
				case 'm':
					time = time*60;
					break;
				case 'h':
					time = time*60*60;
					break;
				case 'j':
					time = time*60*60*24;
					break;
				case 'M':
					time = time*60*60*24*30;
					break;				
			}
			if(time > 0) return time;
		} catch(Exception ex) { }
		return -1;
	}

	public static boolean isServerRpOpen() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM system_parameter WHERE parameter = 'last_actualization_minecraft' AND value+5 >= UNIX_TIMESTAMP();");
			while(result.next()) return true;
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
}
