package fr.shyndard.alteryabungee.function;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MsgManager {

	static Map<UUID, UUID> list = new HashMap<>();
	
	public static void sendMessageTo(ProxiedPlayer player, ProxiedPlayer target) {
		list.put(player.getUniqueId(), target.getUniqueId());
		list.put(target.getUniqueId(), player.getUniqueId());
	}
	
	public static ProxiedPlayer getLastMsgTarget(ProxiedPlayer player) {
		try {
			return BungeeCord.getInstance().getPlayer(list.get(player.getUniqueId()));
		} catch(Exception ex) {
			return null;
		}
		
	}
}
