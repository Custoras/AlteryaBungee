package fr.shyndard.alteryabungee.function;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MuteManager {

	private static Map<UUID, Long> mute_player = new HashMap<>();
	
	public static void add(ProxiedPlayer target, long time) {
		mute_player.put(target.getUniqueId(), (System.currentTimeMillis()/1000)+time);
	}
	
	public static boolean isMute(ProxiedPlayer player) {
		if(mute_player.get(player.getUniqueId()) == null) return false;
		return mute_player.get(player.getUniqueId()) >= System.currentTimeMillis()/1000;
	}
	
	public static String getRemainingTime(ProxiedPlayer player) {
		if(mute_player.get(player.getUniqueId()) == null) return "Rien";		
		Timestamp stamp = new Timestamp(mute_player.get(player.getUniqueId())*1000);
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy � HH:mm:ss");
		Date date = new Date(stamp.getTime());
		return format.format(date).toString();
	}

}
