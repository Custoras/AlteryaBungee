package fr.shyndard.alteryabungee.function;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.method.BanNote;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BanManager {

	public static void add(ProxiedPlayer target, long time, String reason, ProxiedPlayer banner) {
		try {
			String sql = "INSERT INTO player_ban VALUES ((SELECT id FROM player_information WHERE uuid = ?), ?, ?, (SELECT id FROM player_information WHERE uuid = ?), ?)";
			PreparedStatement preparedStatement = Main.getSql().getConnection().prepareStatement(sql);
			preparedStatement.setString(1, target.getUniqueId().toString());
			preparedStatement.setLong(2, (System.currentTimeMillis()/1000));
			preparedStatement.setLong(3, (System.currentTimeMillis()/1000)+time);
			preparedStatement.setString(4, banner.getUniqueId().toString());
			preparedStatement.setString(5, reason);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static boolean isBan(ProxiedPlayer player) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM player_ban WHERE ban_player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') AND ban_end_date > UNIX_TIMESTAMP();");
			while(result.next()) {
				return true;
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public static BanNote getBanNote(ProxiedPlayer player) {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT ban_reason, ban_end_date FROM player_ban WHERE ban_player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') AND ban_end_date > UNIX_TIMESTAMP();");
			while(result.next()) {
				Timestamp stamp = new Timestamp(result.getLong("ban_end_date")*1000);
				DateFormat format = new SimpleDateFormat("dd/MM/yyyy � HH:mm:ss");
				Date date = new Date(stamp.getTime());
				return new BanNote(result.getString("ban_reason"), format.format(date).toString());
        	}
		} catch (SQLException e) {e.printStackTrace();}
		return null;
	}

}
