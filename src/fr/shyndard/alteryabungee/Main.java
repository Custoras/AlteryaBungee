package fr.shyndard.alteryabungee;

import fr.shyndard.alteryabungee.command.CmdAnnonceEvent;
import fr.shyndard.alteryabungee.command.CmdBan;
import fr.shyndard.alteryabungee.command.CmdEvent;
import fr.shyndard.alteryabungee.command.CmdHelp;
import fr.shyndard.alteryabungee.command.CmdKick;
import fr.shyndard.alteryabungee.command.CmdLier;
import fr.shyndard.alteryabungee.command.CmdLierDiscord;
import fr.shyndard.alteryabungee.command.CmdMsg;
import fr.shyndard.alteryabungee.command.CmdMute;
import fr.shyndard.alteryabungee.command.CmdProd;
import fr.shyndard.alteryabungee.command.CmdReply;
import fr.shyndard.alteryabungee.command.CmdSemiRp;
import fr.shyndard.alteryabungee.command.CmdStaff;
import fr.shyndard.alteryabungee.command.CmdStaffChat;
import fr.shyndard.alteryabungee.command.CmdTChat;
import fr.shyndard.alteryabungee.event.PingAction;
import fr.shyndard.alteryabungee.event.PlayerChat;
import fr.shyndard.alteryabungee.event.PlayerConnection;
import fr.shyndard.alteryabungee.event.PlayerKick;
import fr.shyndard.alteryabungee.function.GlobalFunction;
import fr.shyndard.alteryabungee.method.Sql;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;

public class Main extends Plugin {
	
	static Main plugin;
	static Sql sql;
	public static String password = "ThisAndOldPassword";

	public void onEnable() {
		plugin = this;
		
		sql = new Sql("ThisIsAnOldParam", 0, "ThisIsAnOldParam", "ThisIsAnOldParam", "ThisIsAnOldParam");
		
		//FUNCTION//
		GlobalFunction.loadRank();
		
		//COMMAND//
		new CmdAnnonceEvent(this);
		new CmdHelp(this);
		new CmdLier(this);
		new CmdLierDiscord(this);
		new CmdMsg(this);
		new CmdReply(this);
		new CmdStaff(this);
		new CmdBan(this);
		new CmdMute(this);
		new CmdKick(this);
		new CmdStaffChat(this);
		new CmdTChat(this);
		
		new CmdProd(this);
		new CmdEvent(this);
		new CmdSemiRp(this);
		
		//EVENT//
		new PlayerConnection(this);
		new PlayerChat(this);
		new PingAction(this);
		new PlayerKick(this);
		
		Thread WebConnection = new WebConnection(20200);
		WebConnection.start();
	}
	
	public static Main getPlugin() {
		return plugin;
	}
	
	public static Sql getSql() {
		return sql;
	}
	
	public static String getLine(ChatColor color) {
		return ("" + color + ChatColor.STRIKETHROUGH + "                                                                                ");
	}
}
