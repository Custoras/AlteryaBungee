package fr.shyndard.alteryabungee.method;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import fr.shyndard.alteryabungee.APIConnection;
import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PlayerInformation {

	private ProxiedPlayer player;
	private int rank_id;
	
	public PlayerInformation(ProxiedPlayer player) {
		this.player = player;
		actualize(false);
	}
	
	public void actualize(boolean bukkit) {
		loadRank();
		if(bukkit) APIConnection.send("actualizeplayer!�"+player.getName(), player.getServer().getInfo());
	}
	
	private void loadRank() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT rank_id FROM player_information WHERE uuid = '"+player.getUniqueId().toString()+"'");
			while(result.next()) {
        		rank_id = result.getInt("rank_id");
        		return;
        	}
		} catch (SQLException e) {e.printStackTrace();}
		rank_id = 15;
	}
	
	public RankInformation getRank() {
		try {
			return Data.rank_list.get(rank_id);
		} catch(Exception ex) { ex.printStackTrace(); }
		return Data.rank_list.get(15);
	}

	public boolean hasPermission(String perm) {
		if(getRank().getPower() <= 1) return true;
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM "
				+ "( "
					+ "SELECT permission FROM player_permission WHERE player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') "
					+ "UNION " 
			        + "SELECT permission FROM rank_permission WHERE rank_power >= (SELECT power FROM rank_information AS RI JOIN player_information AS PI ON PI.rank_id = RI.id WHERE PI.uuid = '"+player.getUniqueId()+"') "
			    + " ) AS permission_list "
			+ "WHERE permission = '"+perm+"'");
			while(result.next()) return true;
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}

	public boolean isStaff() {
		return (getRank().getPower() <= 5);
	}
}
