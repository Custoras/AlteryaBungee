package fr.shyndard.alteryabungee.method;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import fr.shyndard.alteryabungee.Main;
import fr.shyndard.alteryabungee.function.Data;
import net.md_5.bungee.api.ChatColor;

public class RankInformation {

	private int id;
	private int power;
	private String name;
	private ChatColor color;
	
	public RankInformation(int id) {
		this.id = id;
		actualize();
		Data.rank_list.put(id, this);
	}
	
	public void actualize() {
		loadInformation();
	}
	
	private void loadInformation() {
		try {
			Statement state = Main.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT power, name, chat_format, color, display_rank_tab FROM rank_information WHERE id = " + id);
			while(result.next()) {
				power = result.getInt("power");
				name = result.getString("name");
				color = ChatColor.getByChar(result.getString("color").charAt(0));
        	}
		 } catch (SQLException e) {e.printStackTrace();}
	}
	
	public int getId() {
		return id;
	}
	
	public int getPower() {
		return power;
	}
	
	public String getName() {
		return name;
	}
	
	public ChatColor getColor() {
		return color;
	}
}
