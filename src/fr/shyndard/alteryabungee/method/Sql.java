package fr.shyndard.alteryabungee.method;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

import fr.shyndard.alteryabungee.Main;
import net.md_5.bungee.BungeeCord;

public class Sql {

	private Connection conn;
	private String hostname;
	private Integer port;
	private String database;
	private String user;
	private String password;
	static String prefix = ("[SQL] ");
	
	
	public Sql(String hostname, Integer port, String database, String user, String password) {
		this.hostname = hostname;
		this.port = port;
		this.database = database;
		this.user = user;
		this.password = password;
		connect();
		run();
	}
	
	public Connection getConnection() {
		try {
			if(conn.isClosed() || conn == null) {
				connect();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this.conn;
	}
	
	public Boolean connect() {
		System.out.println(prefix + "Initialization of sql connection...");
		if(conn != null) {try{conn.close();}catch (SQLException e) {e.printStackTrace();}}
		  
	    try {
	    	Class.forName("com.mysql.jdbc.Driver");
	    }
	    catch (ClassNotFoundException e) {
	    	System.out.println(prefix + "Erreur driver, contactez le developpeur");
	    	e.printStackTrace();
	    	return false;
	    }
	    
	    try {
	    	String url = ("jdbc:mysql://" + this.hostname + ":" + this.port + "/" + this.database);
	    	this.conn = DriverManager.getConnection(url, this.user, this.password);
	    }
	    catch (SQLException e) {
	    	System.out.println(prefix + "Erreur de connexion a la base de donnees.");
	    	System.out.println(prefix + "Veuillez v�rifier vos informations de connexion.");
	    	e.printStackTrace();
	    	return false;
	    }
	    System.out.println(prefix + "Sql connection established.");
	    return true;
    }
	
	private void run() {
		BungeeCord.getInstance().getScheduler().schedule(Main.getPlugin(), new Runnable() {
            @Override
            public void run() {
            	try {
					Statement state = getConnection().createStatement();
					ResultSet result = state.executeQuery("SELECT 1 FROM player_information");
					while(result.next()) {
		        		return;
		        	}
				} catch (SQLException e) {e.printStackTrace();}
            }
        }, 1, 5, TimeUnit.MINUTES);
	}
	
}
