package fr.shyndard.alteryabungee.method;

public class BanNote {

	private String reason;
	private String dateRemaining;
	
	public BanNote(String reason, String dateRemaining) {
		this.reason = reason;
		this.dateRemaining = dateRemaining;
	}
	
	public String getReason() { return reason; }
	
	public String getRemaining() { return dateRemaining; }
}
