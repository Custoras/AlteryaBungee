package fr.shyndard.alteryabungee;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import fr.shyndard.alteryabungee.function.Data;
import fr.shyndard.alteryabungee.method.PlayerInformation;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class WebConnection extends Thread {
	
	private ServerSocket serverSocket;
	static String prefix = ("[Listener] ");
	static String game_prefix = ("[" + ChatColor.RED + ChatColor.BOLD + "!" + ChatColor.WHITE + "] " + ChatColor.GRAY);
   
	public WebConnection(Integer port) {
		message("Initialization of the connection on port " + port + "...");
		try {
			serverSocket = new ServerSocket(port);	
		} catch (IOException e) {
			e.printStackTrace();	
			return;
		}
		message("Connection established on port " + port);
	}
	
	void message(String message) {
		System.out.println(prefix + message);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		while (true) {
			try {
				Socket socket = serverSocket.accept();
				socket.setSoTimeout(30*1000);
				DataInputStream in = new DataInputStream(socket.getInputStream());
				BufferedReader br = new BufferedReader( new InputStreamReader(in));
				String message;
				while ((message = br.readLine()) != null) {
					message = message.substring(2);
					String args[] = message.split("!:");
					System.out.println("receive message : " + message);
					if(args.length >= 2) {
						if(args[0].equalsIgnoreCase(Main.password)) {
							if(args[1].equals("actualizeplayer") && args.length == 3) {
								ProxiedPlayer player = BungeeCord.getInstance().getPlayer(args[2]);
								if(player != null) {
									PlayerInformation pi = Data.player_list.get(player.getUniqueId());
									pi.actualize(true);
									player.sendMessage(game_prefix + "Vous devenez " + pi.getRank().getColor() + pi.getRank().getName() + ChatColor.GRAY + ".");
								}
							}
						}
					}
				}
			} catch (IOException ioe) { ioe.printStackTrace(); }
		}
	}	
}